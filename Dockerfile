FROM tomcat:8-jre8

COPY time-tracker-web-0.3.1.war /usr/local/tomcat/webapps/demo.war

EXPOSE 8080

CMD ["catalina.sh", "run"]